﻿#include <iostream>

int main()
{
	int day;
	std::cout << "Enter the day" << "\n";
	std::cin >> day;
	const int N = 5;
	int array[N][N] = {};
	int index(day % N);
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			array[i][j] = i + j;
			std::cout << array[i][j];
		}
		std::cout << "\n";
	}
	std::cout << "\n";

	for (int j = 0; j < N; j++)
	{
		sum += array[index][j];
	}
	std::cout << sum << "\n";

	return 0;
}